memmanage: memmanage.o
	g++ -Wall -o memmanage memmanage.o

memmanage.o: memmanage.cpp
	g++ -Wall -c memmanage.cpp
	
all: memmanage

.PHONY: clean
clean:
	rm -f memmanage.o memmanage

